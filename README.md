# asn-from-barcode
Parse ASN from barcode in consumed documents as a post-consume script in paperless-ngx

## Installation and configuration

Install requirements for python packages: pdftoppm is contained in poppler-utils, zbarimg is contained in libzbar0

For ubuntu, you may use:

`apt install poppler-utils libzbar0`

Install python packages used: pyzbar to parse the barcodes, pdf2image to convert the pdf to png beforehand, psycopg to access the database of paperless-ngx 

`pip install pyzbar pdf2image psycopg[binary]`

Place the script in some meaningful folder, e.g., `/opt/paperless/scripts/parse-asn-for-paperless.py`

Make sure that paperless-ngx has read and execute permissions on the file, e.g.:

`chown paperlessng /opt/paperless/scripts/parse-asn-for-paperless.py && chmod 750 /opt/paperless/scripts/parse-asn-for-paperless.py`

Configure paperless-ngx to execute the script, e.g., by setting `PAPERLESS_POST_CONSUME_SCRIPT=/opt/paperless/scripts/parse-asn-for-paperless.py` in `/etc/paperless.conf` or where you configure your paperless instance.

If you want to use the logging of the script, either adjust the log file configured or create the directory `/var/log/paperless` and make it accessible to the paperless-ngx user.

Fine tune the code to your needs, e.g., by adjusting the relevant code types (CODE128 by default). The valid values depend on zbar.
Also you can provide a regular expression which the barcode must match to be considered an ASN. By default this is set to be an integer number.

## Adjusting the barcode regular expression

You can modify the regular expression the barcode must match by altering the `BARCODE_REGULAR_EXPRESSION` constant.
There is only one requirement the regular expression must fulfill: It must have a named capture group `ASN`.

By default the regular expression is set to `^(?P<ASN>[0-9]+)$`.

To match barcodes in the format `ASN00001` for example the regular expression would be `^ASN(?P<ASN>[0-9]{5})$`.

## Credits
Thanks to muued (https://github.com/muued) the original creator of this project. See https://github.com/muued/asn-from-barcode for his project.

## License
This project is licensed under the `GNU GENERAL PUBLIC LICENSE Version 3`